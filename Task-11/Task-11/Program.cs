﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_11
{
    class Program
    {
        static void Main(string[] args)
        {
            int year;

            Console.WriteLine("Please enter a year to check it is a leap year.");
            year = int.Parse(Console.ReadLine());

            if (year % 400 == 0)
            {
                Console.WriteLine($"The year {year} is a Leap year");
            }
            else if (year % 100 == 0)
            {
                Console.WriteLine($"The year {year} is not a Leap Year");
            }
            else if (year % 4 == 0)
            {
                Console.WriteLine($"The year {year} is a Leap Year");
            }
            else
            {
                Console.WriteLine($"The year {year} is not a Leap Year");
            }
        }
    }
}
