﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_20
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new int[7] {34, 45, 21, 44, 67, 88, 86};
            var odd = new List<int> { };

            foreach (int i in a)
            {
                if (i % 2 != 0)
                odd.Add(i);
                
            }

            var output = string.Join(",", odd);
            Console.WriteLine(output);
        }
    }
}
