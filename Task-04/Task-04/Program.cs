﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            const double c = 1.8;
            const double f = 0.5556;
            var convert = "";
            double deg = 0;

            Console.WriteLine("Please enter Cel to convert from Fahrenheit to Celcius. Please enter Far to convert from Celcius to Fahrenheit");
            convert = Console.ReadLine();


            switch (convert)
            {
                case "Cel":
                    Console.WriteLine("Please enter the temperature in Fahrenheit.");
                    deg = double.Parse(Console.ReadLine());
                    Console.WriteLine($"{deg} \x00B0F is {(deg - 32) * f} \x00B0C");
                    break;

                case "Far":
                    Console.WriteLine("Please enter the temperature in Celcius");
                    deg = double.Parse(Console.ReadLine());
                    Console.WriteLine($"{deg} \x00B0C is {deg * c + 32} \x00B0F");
                    break;

                default:
                    Console.WriteLine("The Selection was invalid");
                    break;
            }
        }
    }
}