﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            const double kilo = 1.609344;
            const double mile = 0.621371;
            var convert = "";
            double dist = 0;

            Console.WriteLine("Please select KM to convert from Miles to KM. Please enter Miles to convert from KM to Miles.");
            convert = Console.ReadLine();        
            

            switch (convert)
            {
                case "KM":
                    Console.WriteLine("Please enter the distance in Miles");
                    dist = double.Parse(Console.ReadLine());
                    Console.WriteLine($"{dist} Miles is {dist * kilo} KM");
                    break;

                case "Miles":
                    Console.WriteLine("Please enter the distance in Kilometers");
                    dist = double.Parse(Console.ReadLine());
                    Console.WriteLine($"{dist}KM is {dist * mile} Miles");
                    break;

                default:
                    Console.WriteLine("The Selection was invalid");
                    break;

            }


        

        }
    }
}
