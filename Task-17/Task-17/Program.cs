﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = new List<Tuple<string, int>> { };
            var input = "";
            var age = 0;
            var i = 0;
            var count = 3;
            Console.WriteLine($"You will be asked to enter 3 peoples names and ages");

            do
            {
                Console.WriteLine($"Please enter your name");
                input = Console.ReadLine();
                Console.WriteLine($"Please enter your age");
                age = int.Parse(Console.ReadLine());
                name.Add(Tuple.Create(input, age));
                i++;
            }
            while (i < count);

            var output = string.Join(",", name);
            Console.WriteLine(output);
        }
    }
}
