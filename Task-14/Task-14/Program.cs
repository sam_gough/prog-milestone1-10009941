﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14
{
    class Program
    {
        static void Main(string[] args)
        {
            var num = "";
            var output = 0;
            Console.WriteLine("Please enter the number of TB you wish to convert to GB");
            num = Console.ReadLine();

            bool value = int.TryParse(num, out output);
            if (value)
            {
                Console.WriteLine($"{num}TB is {output * 1024}GB");
            }
            else if (value == false)
            {
                Console.WriteLine("You must enter a number");
            }
            
        }
    }
}
