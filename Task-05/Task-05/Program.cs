﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a time in 24Hr to convert to am/pm time (only enter the hour)");
            var Time = int.Parse(Console.ReadLine());

            if (Time > 12 && Time < 24)
            {
                Console.WriteLine($"The time is {Time - 12}pm");
            }
            else if (Time == 12)
            {
                Console.WriteLine($"The time is 12pm");
            }
            else if (Time < 12)
            {
                Console.WriteLine($"The time is {Time}am");
            }
            else if (Time == 24)
            {
                Console.WriteLine($"The time is 12am");
            }
        }
    }
}
