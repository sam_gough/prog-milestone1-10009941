﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            var Month = "";
            var Day = "";

            Console.WriteLine("Please enter the month you were born!");
            Month = Console.ReadLine();
            Console.WriteLine("Please enter the day you were born!");
            Day = Console.ReadLine();
                       
            Console.WriteLine($"Thank you! You were born on {Day} {Month}.");
        }
    }
}
