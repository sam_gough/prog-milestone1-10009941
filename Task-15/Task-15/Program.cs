﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_15
{
    class Program
    {
        static void Main(string[] args)
        {
            var num = new List<int> { };
            int i = 0;
            int count = 5;
            int input = 0;

            for (i = 0; i < count; i++)
            {
                Console.WriteLine("Please enter a number and press enter");
                input = int.Parse(Console.ReadLine());
                num.Add(input);
            }

            var total = num.Sum();
            Console.WriteLine($"The total is {total}");
        }
    }
}
