﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_22
{
    class Program
    {
        static void Main(string[] args)
        {
            var dict = new Dictionary<String,String>();
            int countf = 0;
            int countv = 0;

            dict.Add("Apple", "Fruit");
            dict.Add("Banana", "Fruit");
            dict.Add("Carrot", "Vegetable");
            dict.Add("Broccoli", "Vegetable");

            foreach (var x in dict)
            {
                if (x.Key == "Apple")
                {
                    Console.WriteLine(x.Key);
                    countf++;
                }
                if(x.Key == "Banana")
                {
                    Console.WriteLine(x.Key);
                    countf++;
                }
                if (x.Key == "Carrot")
                {
                   countv++;
                }
                if (x.Key == "Broccoli")
                {
                    countv++;
                }
            }
            Console.WriteLine($"There are {countf} fruits, and {countv} vegetables in the dictionary");
        }
    }
}
