﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_13
{
    class Program
    {
        static void Main(string[] args)
        {
            double a = 0;
            double b = 0;
            double c = 0;
            double total = 0;
            
            Console.WriteLine("Please enter 3 GST Exclusive amounts(pressing enter between each number)");
            a = double.Parse(Console.ReadLine());
            b = double.Parse(Console.ReadLine());
            c = double.Parse(Console.ReadLine());

            total = (a * 1.15) + (b * 1.15) + (c * 1.15);
            total = Math.Round(total, 2);
            Console.WriteLine($"The total amount is ${total} incl GST");
        }
    }
}
