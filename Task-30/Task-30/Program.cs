﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_30
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = "";
            var b = "";
            int c = 0;
            int d = 0;

            Console.WriteLine("Please enter a number and press enter");
            a = Console.ReadLine();
            c = int.Parse(a);
            Console.WriteLine("Please enter a number and press enter");
            b = Console.ReadLine();
            d = int.Parse(b);
            Console.WriteLine($"When numbers are added as a string = {a + b}");
            Console.WriteLine($"When numbers are added as numbers = {c + d}");
        }
    }
}
