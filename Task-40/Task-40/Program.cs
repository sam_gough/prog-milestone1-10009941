﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_40
{
    class Program
    {
        static void Main(string[] args)
        {
            var day = new String[7] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
            var count = 0;
            var totdays = 31;

            for (var i = 0; i < totdays; i++)
            {
                var days = i % 7;
                var dayofweek = day[days];
                if (dayofweek == "Wednesday")
                {
                    count++;
                }
            }
            Console.WriteLine($"There are {count} Wednesdays in this month");
        }
    }
}
    

