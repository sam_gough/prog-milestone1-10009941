﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            var Name = "";
            var Age = "";

            Console.WriteLine("Please enter your name!");
            Name = Console.ReadLine();
            Console.WriteLine("Please enter your age!");
            Age = Console.ReadLine();

            Console.WriteLine("Your name is " + Name + " and you are " + Age + " years old!");
            Console.WriteLine("Your name is {0} and you are {1} years old!", Name, Age);
            Console.WriteLine($"Your name is {Name} and you are {Age} years old!");

        }
    }
}
