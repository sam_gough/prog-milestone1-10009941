﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_18
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = new List<Tuple<string, int, string>> { };
            var input = "";
            int age = 0;
            var birth = "";
            var i = 0;
            var count = 3;
            Console.WriteLine($"You will be asked to enter 3 peoples names, ages and birthdays");

            do
            {
                Console.WriteLine($"Please enter your name");
                input = Console.ReadLine();
                Console.WriteLine($"Please enter your age");
                age = int.Parse(Console.ReadLine());
                Console.WriteLine($"Please enter your birthday");
                birth = Console.ReadLine();
                name.Add(Tuple.Create(input, age, birth));
                i++;
            }
            while (i < count);

            var output = string.Join(",", name);
            Console.WriteLine(output);
        }
    }
}
