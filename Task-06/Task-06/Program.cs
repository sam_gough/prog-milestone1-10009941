﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_06
{
    class Program
    {
        static void Main(string[] args)
        {
            var count = 4;
            var i = 0;
            int total = 0;
            int num = 0;

            for (i = 0; i <= count; i++)
            {
                Console.WriteLine("Please enter a number to be added to the total");
                num = int.Parse(Console.ReadLine());
                total = total + num;
                Console.WriteLine($"The total is {total}");
            }

        }
    }
}
