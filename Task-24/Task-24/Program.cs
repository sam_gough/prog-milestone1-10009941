﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_24
{
    class Program
    {
        static void Main(string[] args)
        {
            var menu = 4;

            do
            {
                Console.Clear();

                Console.WriteLine($"********************************");
                Console.WriteLine($"*          MAIN MENU           *");
                Console.WriteLine($"*                              *");
                Console.WriteLine($"*    1. Option 1               *");
                Console.WriteLine($"*    2. Option 2               *");
                Console.WriteLine($"*    3. Option 3               *");
                Console.WriteLine($"*                              *");
                Console.WriteLine($"********************************");
                Console.WriteLine();
                Console.WriteLine($"Please select an option");
                menu = int.Parse(Console.ReadLine());

                if (menu == 1)
                {
                    Console.Clear();
                    Console.WriteLine("You Chose 01");
                    Console.WriteLine("Please press m to return to the main menu");
                    var temp = Console.ReadLine();

                    if (temp == "m")
                    {
                        menu = 4;
                    }
                }
                if (menu == 2)
                {
                    Console.Clear();
                    Console.WriteLine("You Chose 02");
                    Console.WriteLine("Please press m to return to the main menu");
                    var temp = Console.ReadLine();

                    if (temp == "m")
                    {
                        menu = 4;
                    }
                }
                if (menu == 3)
                {
                    Console.Clear();
                    Console.WriteLine("You Chose 03");
                    Console.WriteLine("Please press m to return to the main menu");
                    var temp = Console.ReadLine();

                    if (temp == "m")
                    {
                        menu = 4;
                    }
                }
            } while (menu == 4) ;
        }
    }
}
